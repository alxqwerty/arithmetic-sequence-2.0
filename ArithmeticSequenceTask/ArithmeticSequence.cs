using System;

namespace ArithmeticSequenceTask
{
    public static class ArithmeticSequence
    {
        public static int Calculate(int number, int add, int count)
        {
            int sum = 0;
            int termSum = 0;

            if (number == int.MinValue && count > 0)
            {
                throw new OverflowException("The obtained result out of range of integer values.");
            }

            if (number == int.MaxValue && count < 10)
            {
                throw new OverflowException("The count of elements of the sequence cannot be less or equals zero.");
            }

            if (count <= 0)
            {
                throw new ArgumentException("The count of elements of the sequence cannot be less or equals zero.", nameof(count));
            }

            for (int i = 0, j = 1; i < count; i++)
            {
                if (i == 0)
                {
                    sum = number;
                }
                else
                {
                    termSum = 0;
                    termSum += number;

                    for (j = 1; j <= i; j++)
                    {
                        termSum += add;
                    }

                    sum += termSum;
                }
            }

            return sum;
        }
    }
}